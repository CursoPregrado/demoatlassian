package com.edu.upb.clases;

public class Usuario {
	private String nombre;
	private int cedula;
	
public Usuario(String Nombre, int Cedula){
	this.nombre=Nombre;
	this.cedula=Cedula;
}

public String getNombre() {
	return nombre;
}

public int getCedula() {
	return cedula;
}
}
