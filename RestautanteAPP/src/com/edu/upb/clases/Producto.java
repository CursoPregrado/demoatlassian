package com.edu.upb.clases;

public class Producto {
	private String nombre;
	private int precio;
	
	public Producto(String Nombre, int Precio){
		this.nombre=Nombre;
		this.precio=Precio;
	}

	public int getPrecio() {
		return precio;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}

	public String getNombre() {
		return nombre;
	}
	

}
