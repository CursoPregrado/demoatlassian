package com.upb.restautanteapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.content.Intent;

public class MainActivity extends Activity {
	private Button btnMenu;
	private Button btnMapa;
	private Button btnReserva;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_principal);
		btnMenu = (Button)findViewById(R.id.btnMenu);
		btnMapa = (Button)findViewById(R.id.btnMapa);
		btnReserva = (Button)findViewById(R.id.btnReservar);
		
		btnMenu.setOnClickListener(new OnClickListener() {
		    public void onClick(View view){
		    	lanzarMenu(view);
		        //Acciones
		    }
		});
	
		/*btnMapa.setOnClickListener(new OnClickListener() {
		    public void onClick(View view){
		    	lanzarMenu(view);
		        //Acciones
		    }
		});*/
		
		btnReserva.setOnClickListener(new OnClickListener() {
		    public void onClick(View view){
		    	lanzarReserva(view);
		        //Acciones
		    }
		});
	
	
	}

	public void lanzarMenu(View view) {
        Intent i = new Intent(this, MenuActivity.class );
        startActivity(i);
  }    
	
	public void lanzarReserva(View view) {
        Intent i = new Intent(this, ReservaActivity.class );
        startActivity(i);
  }    
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
